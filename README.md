# SecretKey

The SecretKey is a USB key that acts as a [FIDO2][fido2] authenticator and secret storage device.

## Features

- FIDO2 authenticator for Windows Hello, Linux U2F, SSH and [WebAuthn][webauthn]
- Encrypted onboard storage for secrets, such as browser passwords and disk encryption keys
- Secure element - keys cannot be extracted even with (simple) offline hardware attacks
- Hardware-based PIN authentication
- Interactive confirmations (e.g. might display `sudo` command before authentication)

## Hardware

The hardware for v1.1 is designed and PCBs are ready. The next step is to assemble a functional hardware prototype.

- ATmega32u4 AVR microcontroller with built-in USB stack
- ATECC608B secure element for crypto operations and secret storage
- 1Mbit SPI EEPROM for encrypted data storage
- USB-C jack (for use with a cable; the PIN pad would be cumbersome to use when plugged in)
- 10-digit PIN pad + 2 action buttons
- 64x32 0.49" OLED display for interactivity
- WS2812B RGB LED for status indication

![Annotated SecretKey v1.1 PCB](docs/pcb-v1.1.png)

## Software

The software is yet to be written.

The FIDO2 USB stack will be based on the firmware for the [Solo][solo] FIDO2 authenticator, with additions for the secret storage functionality.

## License

The SecretKey hardware is licensed under [CERN-OHL-W v2](LICENSE-CERN-OHL-W) or later.

The SecretKey firmware and any accompanying software will be licensed under the [MIT license](LICENSE-MIT).

## Future versions

An initial v1.2 revision is already designed, including:

- Same v1.x features
- 3.3V logic
- NXP SE050 secure element (better availability, no NDA, 3.3V only)
- STM32L052K6T6 or similar microcontroller (happened to be available, faster, 3.3V only) <!-- Other suitable STM32 F/G models (3.3V, USB, LQFP32/48): STM32F070Cx, STM32F042xx, STM32F10xCx, STM32G0B0CE, STM32G0B1xx -->
- 32 Mbit SPI flash instead of 1 Mbit EEPROM (3.3V only)

SecretKey v2.0 (in the distant future) will be a complete redesign, hopefully including:

- nRF52840 or similar SoC
- Full functionality over Bluetooth for mobile devices
- Full functionality over NFC (likely with a dedicated USB stand)
- Internal battery allowing for wireless use and pre-entering PIN





[fido2]: https://fidoalliance.org/fido2/
[webauthn]: https://webauthn.io/
[solo]: https://github.com/solokeys/solo
